# Vim Config

## Use 

- install Gvim 7.4

- clone this repo

- copy all the files in Vim (except `gvim74.exe`) to Vim installed directory

- install vim plugins

```bash
:PluginInstall
```
