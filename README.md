# Awesome Stars

*:octocat:小越越的星*

## 内容目录

- [持续关注](#持续关注)
- [一、Front-end](#front-end)
- [二、Books](#books)
- [三、blog](#blog)
- [四、Tutorial](#tutorial)
- [五、Inteview](#inteview)
- [六、Tools](#tools)
- [七、资源站](#资源站)
- [八、深度学习](#深度学习)
- [九、编码风格](#编码风格)
- [十、团队](#团队)

 
## 持续关注
- [weekly](https://github.com/dt-fe/weekly) 前端精读周刊
- [feweekly](https://github.com/wangshijun/feweekly) 前端周刊，让你在前端领域跟上时代的脚步，深度和广度不断精进 http://www.feweekly.com

**[⬆ 回到顶部](#内容目录)**

## Front-end

- [How-To-Ask-Questions-The-Smart-Way](https://github.com/ryanhanwu/How-To-Ask-Questions-The-Smart-Way/blob/master/README-zh_CN.md#%E5%9C%A8%E6%8F%90%E9%97%AE%E4%B9%8B%E5%89%8D) 提问的智慧
- [README](https://github.com/guodongxiaren/README) Github Flavored Markdown语法 README
- [developer-roadmap](https://github.com/kamranahmedse/developer-roadmap) - 2018年路线图成为web开发人员
- [developer](https://github.com/phodal/developer) - 开发者路线图 使用 [growth](https://github.com/phodal/growth) 代替 [this](http://developer.phodal.com/) 
- [fks](https://github.com/JacksonTian/fks) - 前端技能汇总 http://html5ify.com/fks/
- [2016 年崛起的 JS 项目](https://risingstars.js.org/2016/zh) / [2017年 JavaScript 明星项目 ](https://risingstars.js.org/2017/zh) / [bestof.js.org](https://bestof.js.org/)

**[⬆ 回到顶部](#内容目录)**


## Books

- [free-programming-books](https://github.com/EbookFoundation/free-programming-books/blob/master/free-programming-books-zh.md) / [free-programming-books-zh_CN](https://github.com/justjavac/free-programming-books-zh_CN)  - 免费的编程书籍 
- [You-Dont-Know-JS](https://github.com/getify/You-Dont-Know-JS) / [Functional-Light-JS](https://github.com/getify/Functional-Light-JS)
- [booktree](https://github.com/phodal/booktree) A Book Tree http://phodal.github.io/booktree/
- [books](https://github.com/programthink/books) 【编程随想】收藏的电子书清单（多个学科，含下载链接）

**[⬆ 回到顶部](#内容目录)**

## blog


- [mqyqingfeng](https://github.com/mqyqingfeng) / [Blog](https://github.com/mqyqingfeng/Blog) 
冴羽写博客的地方，预计写四个系列：JavaScript深入系列、JavaScript专题系列、ES6系列、React系列。
- [creeperyang](https://github.com/creeperyang) / [blog](https://github.com/creeperyang/blog/issues) 前端博客，关注基础知识和性能优化
- [hpoenixf](https://github.com/hpoenixf) / [hpoenixf.github.io](https://github.com/hpoenixf/hpoenixf.github.io) 博客文章，含前端进阶系列
- [atian25](https://github.com/atian25) / [blog](https://github.com/atian25/blog) 天猪部落阁 http://atian25.github.io
- [sunyongjian](https://github.com/sunyongjian) / [blog](https://github.com/sunyongjian/blog/issues) 个人博客 :stuck_out_tongue_closed_eyes::yum::smile:

**[⬆ 回到顶部](#内容目录)**
 
## Tutorial

### 综合
- [Web-Series](https://github.com/xiaoyueyue165/Web-Series) 现代 Web 开发，现代 Web 开发导论 | 基础篇 | 进阶篇 | 架构优化篇 | React 篇 | Vue 篇 https://parg.co/bMe

### javascript

- [jstutorial](https://github.com/ruanyf/jstutorial) Javascript tutorial book http://javascript.ruanyifeng.com

### typescript

- [官方手册](https://legacy.gitbook.com/book/zhongsp/typescript-handbook/details)
- [typescript-tutorial](https://github.com/xcatliu/typescript-tutorial) TypeScript 入门教程 https://ts.xcatliu.com/

### 源码分析

- [jQuery-](https://github.com/chokcoco/jQuery-) jQuery源码解读 -- jQuery v1.10.2
- [underscore-analysis](https://github.com/anzichi/underscore-analysis) Underscore.js 源码解读 & 系列文章（完 ❗️） https://git.io/vNN8y
- [vue2.0-source](https://github.com/liutao/vue2.0-source) vue源码分析 -- 基于 2.2.6版本
- [learnVue](https://github.com/answershuto/learnVue) :octocat:Vue.js 源码解析 https://github.com/answershuto/learnVue
- [mvvm](https://github.com/DMQ/mvvm) 剖析vue实现原理，自己动手实现mvvm

### nodejs

- [N-blog](https://github.com/nswbmw/N-blog) 《一起学 Node.js》
- [node-in-debugging](https://github.com/nswbmw/node-in-debugging) 《Node.js 调试指南》

### java

- [java-learning](https://github.com/brianway/java-learning) 旨在打造在线最佳的 Java 学习笔记，含博客讲解和源码实例，包括 Java SE 和 Java Web
- [Java](https://github.com/DuGuQiuBai/Java) 27天成为Java大神

### mysql
- [mysql-tutorial](https://github.com/jaywcjlove/mysql-tutorial) 
MySQL入门教程（MySQL tutorial book）

**[⬆ 回到顶部](#内容目录)**

## Inteview

- [Interview-Notebook](https://github.com/CyC2018/Interview-Notebook)  技术面试需要掌握的基础知识整理
- [front-end-interview-handbook](https://github.com/yangshun/front-end-interview-handbook) 几乎完整的答案可以用来面试潜在的候选人，测试自己或完全忽略“前端工作面试问题”
- [node-interview](https://github.com/ElemeFE/node-interview) How to pass the Node.js interview of ElemeFE. https://elemefe.github.io/node-interview/

**[⬆ 回到顶部](#内容目录)**

## Tools

- [toolbox](https://github.com/phodal/toolbox) - 一个全栈工程师的工具箱
- [Awesomes.cn ](https://www.awesomes.cn/) - 也许是最好的开源前端库、框架和插件集合 」
- [loupe](https://github.com/latentflip/loupe) 可视化的javascript运行时在运行时 http://latentflip.com/loupe
- [firefly-proxy](https://github.com/yinghuocho/firefly-proxy) A proxy software to help circumventing the Great Firewall.
- [doxmate](https://github.com/JacksonTian/doxmate) - 文档伴侣 http://html5ify.com/doxmate
- [Pandoc](http://www.pandoc.org/) 一个通用的文档转换器
- [小熊免费文件格式在线转换工具](https://www.ofoct.com/zh/)
- [media.io](http://media.io/) / [Bear File Converter – Online & Free](https://www.bearaudiotool.com/wav-to-mp3) 音频转换器
- [fh5co](https://freehtml5.co/author/fh5co/) 快速建站 /[推荐网站](http://www.chinaz.com/website/) /[网站排行](http://top.chinaz.com/)

**[⬆ 回到顶部](#内容目录)**


## 资源站
- [free-programming-books](https://github.com/EbookFoundation/free-programming-books/blob/master/free-programming-books-zh.md) books Freely available programming books
- [Front-end-tutorial](https://github.com/windiest/Front-end-tutorial) 😺猫的前端回忆录 
- [gold-miner](https://github.com/xitu/gold-miner) 掘金翻译计划  https://juejin.im/tag/掘金翻译计划
- [wohugb](https://github.com/wohugb/wohugb.github.com) 巴别塔----------大鼻子文档分享 http://wohugb.github.io/
- [Awsome-Front-End-learning-resource](https://github.com/helloqingfeng/Awsome-Front-End-learning-resource) - [:octocat:GitHub最全的前端资源汇总仓库（包括前端学习、开发资源、求职面试等）](https://helloqingfeng.github.io/front-end-index/index.html)
- [awesome-javascript-cn](https://github.com/jobbole/awesome-javascript-cn) 伯乐JavaScript 资源大全中文版

**[⬆ 回到顶部](#内容目录)**

## 编码风格

- [project-guidelines](https://github.com/wearehive/project-guidelines/blob/master/README-zh.md) - JS 项目行军指南
- [javascript](https://github.com/airbnb/javascript) - Javascript风格指导
- [You-Dont-Need-jQuery](https://github.com/nefe/You-Dont-Need-jQuery/blob/master/README.zh-CN.md) - 你不需要使用jquery
- [clean-code-javascript](https://github.com/ryanmcdermott/clean-code-javascript) - 干净的概念用于JavaScript代码
- [frontend-guidelines](https://github.com/bendc/frontend-guidelines) - Some HTML, CSS and JS best practices.
- [TGideas](http://tguide.qq.com/main/index.htm) TGideas整体WEB解决方案 

**[⬆ 回到顶部](#内容目录)**

## 深度学习

- [Screenshot-to-code-in-Keras](https://github.com/emilwallner/Screenshot-to-code-in-Keras) -将设计图转化为静态网页的神经网络
- [brain](https://github.com/harthur/brain) -简单的javascript神经网络
- [deeplearnjs](https://github.com/PAIR-code/deeplearnjs) -硬件加速的深度学习//机器学习//网络的NumPy库

**[⬆ 回到顶部](#内容目录)**



## 团队
### article

- [奇舞周刊](https://weekly.75team.com/)
- [百度 FEX 周刊](http://fex.baidu.com/weekly/)
- [w3ctech ](https://www.w3ctech.com/)
- [w3cplus](https://www.w3cplus.com/)
**[⬆ 回到顶部](#内容目录)**

### github

- [Microsoft](https://github.com/Microsoft) 
- [Facebook](https://github.com/facebook?utf8=%E2%9C%93&q=&type=&language=javascript) 
- [ElemeFE](https://github.com/ElemeFE)
- [Alibaba](https://github.com/alibaba) 
- [Tencent](https://github.com/AlloyTeam)

### web

- [TaobaoUED](http://taobaofed.org/categories/Web%E5%BC%80%E5%8F%91/)
- [百度EFE（Excellent FrontEnd）](http://efe.baidu.com/)
- [AlloyTeam](http://www.alloyteam.com/) （腾讯）
- [饿了么前端](https://zhuanlan.zhihu.com/ElemeFE)
- [IMWeb](http://imweb.io/) （腾讯）
- [大搜车](http://f2e.souche.com/blog/) or [Xin](https://blog.souche.com/)

**[⬆ 回到顶部](#内容目录)**







